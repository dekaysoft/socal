Primero Uno:::::::
	At the end of each section, I committed the code to the gitlab repository.  
	If you want to compare your code to mine, please make sure you use the commit that matches the section!

Section 2 notes
*************************************************
ERROR in node_modules/rxjs/internal/types.d.ts(81,44): error TS1005: ';' expected.
	1. Edit package.json, changing the following line
		"rxjs": "^6.0.0",
		to
		"rxjs": "6.0.0",
	2. Stop "ng serve" (if you haven't already)
	3. npm i
		** rxjs released an update 6.0.4 that caused this to break.
		** We need to tell it to use 6.0.0, not the lastest version of 6

-------------------------------------------------------------
value.component.ts shows an error immediately after generation...
	"value.component.ts * 1 problem in this file * untracked"
	it doesn't prevent the "ng serve" from compiling successfully,
		and no detail is shown regarding the error.
	** continue the tutorial - it goes away after a while

--------------------------------------------------------------
Turned off the auto-save, as the constant errors displayed 
	while I was in the middle of editing proved distracting.

--------------------------------------------------------------
Reverted bootstrap to 4.1.1, and ran "npm i" to apply it

--------------------------------------------------------------
I use TortoiseGit to handle pushing my Git commits up to gitlab, 'caused I wasn't able
to easily figure out how to configure Visual Studio Code to use SSH keys when pushing
to GitLab.

Section 3 notes:
https://gitlab.com/dekaysoft/socal/tree/e70503a564ad0b1a0b40b06df061f46625bfc051
*******************************************************
No corrections needed - just needed to pay attention to the tutorial, and not leave things out :D

Section 4 notes:
https://gitlab.com/dekaysoft/socal/tree/43bcea6d7952abdca9bd3c90265255d587a7f959
*******************************************************
Downloaded Bootstrap v4.1 instead of the latest

-------------------------------------------------------
He goes really fast in this section - changes between files quickly, and I guess he expects
you to be typing as he does.  Be prepared to pause and rewind a lot!

Section 5 notes:
https://gitlab.com/dekaysoft/socal/tree/c716031382daacdf7c153cb0a40bac651ed66f03
*******************************************************
The only change I made was using "modelStateErrors" instead of "modalStateErrors" - I'm ceratin he meant "model", not "modal".

Section 6 notes:
https://gitlab.com/dekaysoft/socal/tree/d6278208e63acddf0ed93130aa464a4b25f14784
*******************************************************
Had to install the npm packages using the external PowerShell, rather than the terminal inside Visual Studio Code

updated package.json using 

    "@auth0/angular-jwt": "2.0.0",
    "alertifyjs": "1.11.1",
	"ngx-bootstrap": "3.0.1",	
    "bootswatch": "4.1.1",
	
and ran npm i
------------------------------------------------------
At the end of Section 6, session 55, the instructor demonstrated that clicking "Log Out", then logging in as a second user
would display the name of the 2nd user.

However, in my version of node (9.3.0), I had to hit Refresh to erase the first user's token. If I did not, the auth.service.ts
would contain the decodedToken of the first user, and would not update the user's name.

So, I added a code block to auth.service.ts to set/unset the decodedToken based in the loggedIn method.

				___________________________________________________________________________________________________________________________________
				___________________________________________________________________________________________________________________________________
				loggedIn() {
					const token = localStorage.getItem('token');
					// The Udemy instructor didn't have to do this next line, but I don't understand how his version appeared to work.
					// So I added this next block to set the authService.decodedToken on loggedIn based on whether the token has expired or not.
					if (this.jwtHelper.isTokenExpired(token)) {
					this.decodedToken = null;
					} else {
					this.decodedToken = this.jwtHelper.decodeToken(token);
					}
					return !this.jwtHelper.isTokenExpired(token);
				}
				__________________________________________________________________________________________________________________________________
				__________________________________________________________________________________________________________________________________


Section 7 notes:
https://gitlab.com/dekaysoft/socal/tree/72cd03e881c99a7e9db6650319e1e08ea28a2983
************************************************************
While the instructor got an error like 

"Expected ',' instead of ''"

I actually got the first user's records.  When I continued watching the video, I found that I was getting the same "self-referencing" error that he was getting
-----------------------------------------------------------------

Section 8 notes:
https://gitlab.com/dekaysoft/socal/tree/9437a2e526ebe4aa811bf6b8bd6872deb001a228
***********************************************************

Secion 9 notes:
https://gitlab.com/dekaysoft/socal/tree/dbcb5773dcebf64adb931f13d331c72901c595ce
***********************************************************
	Moving "member-list" folder to "members" folder caused Visual Studio Code to prompt me to automatically update the imports.  I selected yes, then watched the
		instructor to see how to do it manually.  Had to make a small change to member-list.component.ts to remove the error with "@Component"

	Card Component not showing in the Angular Bootstrap documentation online for me... just watched the instructor.

	If you did what I did in Section 6 to fix the login/logout difference
			"So, I added a code block to auth.service.ts to set/unset the decodedToken based in the loggedIn method."
		Then you won't see the error that he sees when logging out/logging back in.
	After finishing the member-detail component, I noticed that my images were being squished.  So, I moved the img reference
		to be a child of card-body, instead of card to make the images not be squished.
			-- His Code --
			<div class="card">
				<img class="card-img-top img-thumbnail" src="{{user?.photoUrl}}" alt="{{user?.knownAs}}">
				<div class="card-body">
					
			-- My Code --
			<div class="card">
				<div class="card-body">
					<img class="card-img-top img-thumbnail" src="{{user?.photoUrl}}" alt="{{user?.knownAs}}">
		However, now my user details don't line up under the left of the image... I'm going to leave this alone,
		and maybe take a class on css after I'm comfortable with Angular :D
      
Section 10 notes:
https://gitlab.com/dekaysoft/socal/tree/5790055d99bc7dca1b417ac47afd9c36e737c77a
***********************************************************


Secion 11 notes:
https://gitlab.com/dekaysoft/socal/tree/718eeef6adc48f0b863d59d9c52e5ed8af9c2452
***********************************************************
The Cloudinary website/documentation is significantly different from what the instructor is showing,
	so I just follwed along with him.  I did, however, create a free account :D

Installing the CloudinaryDotNet didn't work the same way as the instructor showed... I copied and pasted "CloudinaryDotNet"
	but had to press <Enter> twice before the list of versions came up.  I selected 1.3.1 (same as the instructor).
	Got the exact same warning that he says to ignore...

	Session 107: If you only get one image at the end of this session, and you used my code
		for member-detail.component.ts from an earlier commit... I had the 
			"return imageUrls;"
		inside the for loop, so the method "getImages()" was only returning one image
		in the member-detail view.  Sorry 'bout that... fixed in this commit.

Section 12 notes:
https://gitlab.com/dekaysoft/socal/commit/3b2926e2686a8e0c2917b4192a19c776506ca0b4
******************************************************************

Section 13 notes:
https://gitlab.com/dekaysoft/socal/commit/7585a658cdc0032cfc2b8657e12ab006f89b4617
******************************************************************
After I added the " | timeAgo" to member-detail.component.html, I got the following error:
	The pipe 'timeAgo' could not be found.
Yet, the "ng serve" compiled just fine, and the date was formatted correctly.  Just going to ignore it.

Section 14 notes:
https://gitlab.com/dekaysoft/socal/commit/cfafc49c8134e2a63dcef94d5d5420682bc89232
******************************************************************
When I executed "GetUsers" in Postman in episode 139, I got the following error:
	"The input was not valid."
Instead of what the instructor got.  Didn't seem to matter, though.


Section 15 notes:
https://gitlab.com/dekaysoft/socal/commit/4648c639ed931e776dd149b3ac8e7880293ee04f
******************************************************************
Because I've been modifying the code to not be so gender-restrictive, I've had to deviate from the instructor's
example.  As a result, I had to choose whether to spend more time on refitting the application to accomodate
my changes, or continue on with the course.  So I chose to continue on, and removed the ability to "filter by multiple genders".

Huh - in only just now occurred to me that the password field was not hiding the input... changed the control type to "password".

Section 16 notes:
https://gitlab.com/dekaysoft/socal/commit/848041f030d14002c506aed7fb1010258d891e43
******************************************************************

Section 17 notes:

******************************************************************
Video #177: I didn't notice any difference in the alertify css animation...
Video #180: What a cluster-fuck... the dotnet website looks totally different, and I don't have the same download options that the instructor is demonstrating...
	So I took my best guess.  I'm so not a fan of this code-first way of building the database...
Video #181: After you seed the data, don't forget to comment out the "seeder.SeedUsers();"!
	Otherwise, when you restart the server in Video #182, you'll get duplicates of all of your users
	Ask me how I learned that :D
	Alternatively, you could put the line in that I left out... "if (!_context.Users.Any())"
Video #183: I diverged significantly from the instructor, because I wanted to install the website on my Intel NUC webserver, which already has the domain dekaysoft.com...