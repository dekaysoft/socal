using System;

namespace SoCal.Api.Dtos
{
    public class PhotoForReturnDto
    {
        // I wonder why he didn't have PhotoForReturnDto implement PhotoForDetailedDto
        public int Id { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsMain { get; set; }
        public string PublicId { get; set; }
    }
}